/*
  DigitalReadSerial

  Reads a digital input on pin 2, prints the result to the Serial Monitor

  This example code is in the public domain.

  http://www.arduino.cc/en/Tutorial/DigitalReadSerial
*/

// digital pin 2 has a pushbutton attached to it. Give it a name:
#define pinDnBtn 7
#define pinUpBtn 4
#define pinCCBtn 6 
#define pinPMBtn 5 
#define pinCCLgt 9
#define pinPMLgt 8


#define T_dbc 200
unsigned long currMillis;
unsigned long lastMillis;
unsigned long dt_ms  = 0; 

int counter = 0; 

bool statUpBtn = 0; bool zstatUpBtn = 0; int durUpBtn = 0; bool alrdyDbcUpBtn = 0; 
bool statDnBtn = 0; bool zstatDnBtn = 0; int durDnBtn = 0; bool alrdyDbcDnBtn = 0; 
bool statCCBtn = 0; bool zstatCCBtn = 0; int durCCBtn = 0; bool alrdyDbcCCBtn = 0; 
bool statPMBtn = 0; bool zstatPMBtn = 0; int durPMBtn = 0; bool alrdyDbcPMBtn = 0; 

bool statCCLed = 0; 
bool statPMLed = 0; 

void setup() {
  Serial.begin(2000000);

  pinMode(pinCCLgt, OUTPUT);
  pinMode(pinPMLgt, OUTPUT);
  pinMode(pinUpBtn, INPUT_PULLUP); 
  pinMode(pinDnBtn, INPUT_PULLUP); 
  pinMode(pinCCBtn, INPUT_PULLUP); 
  pinMode(pinPMBtn, INPUT_PULLUP);   
  lastMillis = millis(); 
}

void loop() {
  currMillis = millis(); 
  dt_ms = currMillis - lastMillis; 
  lastMillis = currMillis;   

  zstatUpBtn = statUpBtn; statUpBtn = !digitalRead(pinUpBtn); 
  zstatDnBtn = statDnBtn; statDnBtn = !digitalRead(pinDnBtn); 
  zstatCCBtn = statCCBtn; statCCBtn = !digitalRead(pinCCBtn);
  zstatPMBtn = statPMBtn; statPMBtn = !digitalRead(pinPMBtn);

  // debounce CC btn and output LED
  if (statCCBtn & zstatCCBtn & !alrdyDbcCCBtn) { // logic HI and not an edge
    durCCBtn = durCCBtn + dt_ms; 
    if (durCCBtn > T_dbc) {
      statCCLed ? statCCLed = 0 : statCCLed = 1 ; // flip it
      digitalWrite(pinCCLgt, statCCLed);
      alrdyDbcCCBtn = true;    
    }
  } else if (!statCCBtn & zstatCCBtn) { //reset flags and vars if falling edge
    alrdyDbcCCBtn = false; 
    durCCBtn = 0;  
  } 

  // debounce PM btn and output LED
  if (statPMBtn & zstatPMBtn & !alrdyDbcPMBtn) { // logic HI and not an edge
    durPMBtn = durPMBtn + dt_ms; 
    if (durPMBtn > T_dbc) {
      statPMLed ? statPMLed = 0 : statPMLed = 1 ; // flip it
      digitalWrite(pinPMLgt, statPMLed);
      alrdyDbcPMBtn = true;    
    }
  } else if (!statPMBtn & zstatPMBtn) { //reset flags and vars if falling edge
    alrdyDbcPMBtn = false; 
    durPMBtn = 0;  
  } 
  
  Serial.println("dt_ms :" + String(dt_ms) + " durCCBtn: " + String(durCCBtn) + " statCCLed: " + String(statCCLed)); 
  //Serial.println("UpBtn: " + String(statUpBtn) + " DnBtn: " + String(statDnBtn) + " CCBtn: " + String(statCCBtn) + " PMBtn: " + String(statPMBtn)); 
}
