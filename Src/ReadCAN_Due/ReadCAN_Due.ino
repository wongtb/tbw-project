// Arduino Due - Displays all traffic found on either canbus port
// By Thibaut Viard/Wilfredo Molina/Collin Kidder 2013-2014

// Required libraries
#include "variant.h"
#include <due_can.h>

//Leave defined if you use native port, comment if using programming port
//This sketch could provide a lot of traffic so it might be best to use the
//native port
//#define Serial SerialUSB
#define CANID_VEHCHAS 1552  //0x610
#define CANID_BRK     548   //0x224
#define CANID_PRNDL   948   //0x3B4
#define CANID_APPS    947   //0x3B3  

int     TestVal = 0; 
int     VehSpd = 0; 
float   AclPos = 0; 
bool    BrkAct = false; 
bool    Gear_D = false;
bool    Gear_R = false;
bool    Gear_P = false;
bool    Gear_N = false; 

void setup()
{

  Serial.begin(115200);
  
  // Initialize CAN0 and CAN1, Set the proper baud rates here
  Can0.begin(CAN_BPS_500K);
  //Can1.begin(CAN_BPS_500K);
  Serial.print("hello");
  
  //By default there are 7 mailboxes for each device that are RX boxes
  //This sets each mailbox to have an open filter that will accept extended
  //or standard frames
  int filter;
  //extended
  for (filter = 0; filter < 3; filter++) {
	//Can0.setRXFilter(filter, 0, 0, true);
	//Can1.setRXFilter(filter, 0, 0, true);
  }  
  //standard
  for (int filter = 3; filter < 7; filter++) {
	//Can0.setRXFilter(filter, 0, 0, false);
	//Can1.setRXFilter(filter, 0, 0, false);
  }  

  // chassis
  Can0.setRXFilter(0, CANID_VEHCHAS, 0xFFFFFFFF, false);
  
  // brake
  Can0.setRXFilter(1, CANID_BRK, 0xFFFFFFFF, false);
  
  // prndl
  Can0.setRXFilter(2, CANID_PRNDL, 0xFFFFFFFF, false);

  // APPS
  Can0.setRXFilter(2, CANID_APPS, 0xFFFFFFFF, false);
  
}

void printFrame(CAN_FRAME &frame) {
   Serial.print("ID: 0x");
   Serial.print(frame.id, HEX);
   Serial.print(" Len: ");
   Serial.print(frame.length);
   Serial.print(" Data: 0x");
   
   for (int count = 0; count < frame.length; count++) {
       Serial.print(frame.data.bytes[count], HEX);
       Serial.print(" ");
   }
   Serial.print("\r\n");
}


void decipherFrame(CAN_FRAME &frame) {
   if (frame.id == CANID_VEHCHAS) {
      VehSpd = frame.data.bytes[2]; 
   } else if (frame.id == CANID_BRK){
      BrkAct = frame.data.bytes[0] << 5; 
   } else if (frame.id == CANID_PRNDL) {
      Gear_D = frame.data.bytes[5] << 5; // Only D - no D3, 2 or L
   } else if (frame.id == CANID_APPS) {
      AclPos = (frame.data.bytes[2] - 30.00) * 0.75; 
   }
   
   Serial.print("\r\n");
}

void loop(){
  CAN_FRAME incoming;
  
  if (Can0.available() > 0) {
	  Can0.read(incoming); 
	  //printFrame(incoming);
    decipherFrame(incoming); 
  }
  Serial.print("Brake Active: "); 
  Serial.print(String(BrkAct));
  Serial.print(" | Accel Pedal: "); 
  Serial.print(String(AclPos));
  Serial.print("% | Drive Active: "); 
  Serial.print(String(Gear_D));
  Serial.print(" | Vehicle Spd: ");
  Serial.print(String(VehSpd));  
  Serial.print("kph \n"); 
}
