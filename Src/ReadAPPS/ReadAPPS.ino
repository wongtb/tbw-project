#include "MCP4725.h"
#include "Wire.h"

int pinADC1 = A0;
int pinADC2 = A1; 
//int pinPdlO1 = A14;
//int pinPdlO2 = A15; 


// Pedal Constants
#define VMIN1 0.7869
#define VMIN2 1.5885 
#define VSLOPE1 33.9315 
#define VSLOPE2 33.7067

// Pedal Mapping Table
#define LUT_Size 9 
float LUT_Brkpts[LUT_Size]  = {0, 5, 10,  20,  30,  40,  50,  60,  100};
float LUT_Val[LUT_Size]     = {0, 2, 4, 6, 8, 10, 11, 14, 100};  

MCP4725 MCP1(0x60); 
MCP4725 MCP2(0x61); 

void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200); 
  MCP1.begin();
  MCP2.begin();
  //Serial.println(String(MCP.isConnected()));     
}

void loop() {
  // put your main code here, to run repeatedly: 
  
  // Obtain ADC Voltages
  float Vaps1 = analogRead(pinADC1) / 1023.0 * 5; 
  float Vaps2 = analogRead(pinADC2) / 1023.0 * 5;

  // Calculate Pedal Position
  float ActApps1 = (Vaps1 - VMIN1) * VSLOPE1;  // %
  float ActApps2 = (Vaps2 - VMIN2) * VSLOPE2;  // %`
  float ActApps = (ActApps1 + ActApps2) / 2;      // Take average
  float ModApps = 0;                              // %
  
  // Pedal Mapping
  if (ActApps >= LUT_Brkpts[LUT_Size-1]) {      // Clip at maximum
    ModApps = LUT_Val[LUT_Size-1]; 
  }
  else if (ActApps <= LUT_Brkpts[0]) {          // Clip at minimum
    ModApps = LUT_Val[0]; 
  }
  else {                                        // Interpolate
    for (int x = 0; x < LUT_Size; x=x+1){
      if (ActApps >= LUT_Brkpts[x] && ActApps <= LUT_Brkpts[x+1]) {
        ModApps = LUT_Val[x] + (LUT_Val[x+1]-LUT_Val[x])*(ActApps-LUT_Brkpts[x])/(LUT_Brkpts[x+1]-LUT_Brkpts[x]);        
      }
    }
  }
 // ModApps = ActApps; 

  // Convert Output Apps to Voltage
  float o_Vaps1 = VMIN1 + ModApps / VSLOPE1;   
  float o_Vaps2 = VMIN2 + ModApps / VSLOPE2;   

  // Write RAW ADC voltage DAC
  MCP1.setValue(1 / 4.8* 4095);
  MCP2.setValue(o_Vaps1 / 5.0 * 4095); 

  //MCP1.setValue(Vaps2 / 5.0 * 4095);
  //MCP2.setValue(Vaps1 / 5.0 * 4095); 

  
  // Read DAC votlages;
//  float o_Vaps1_act = analogRead(pinPdlO1) / 1023.0 * 5; 
//  float o_Vaps2_act = analogRead(pinPdlO2) / 1023.0 * 5; 

  // Print estimated ActApps
  String printString = "The read voltages are : " + String(Vaps1, 4) + "V (" + String(ActApps1) + "%) / " + String(Vaps2,4) + "V (" + String(ActApps2) + "%) || ";
  String printString3 = "The Apps is " + String(ActApps) + "% || ";
  String printString4 = "The Modified Apps is " + String(ModApps) + "% "; 
  String printString5 = "The output voltages are : " + String(o_Vaps1, 3) + "V / " + String(o_Vaps2, 3) + "V. || "; 
  Serial.println(printString  + printString3 + printString5 + printString4); 

//  Serial.println("Hello"); 

  //delay(1000); 
}
