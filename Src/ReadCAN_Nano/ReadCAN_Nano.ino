// CAN Variables
#include <mcp_can.h>
#include <SPI.h>
#define CANID_VEHCHAS   1552  //0x610
#define CANID_VEHCHAS_F 176   //0xb0
#define CANID_BRK       548   //0x224
#define CANID_PRNDL     948   //0x3B4
#define CANID_APPS      947   //0x3B3  

int     VehSpd = 0; 
float   VehSpd2 = 0.0; 
float   AclPos = 0.0; 
bool    BrkAct = false; 
bool    Gear_D = false;
bool    Gear_R = false;
bool    Gear_P = false;
bool    Gear_N = false; 

long unsigned int rxId;
unsigned char len = 0;
unsigned char rxBuf[8];
MCP_CAN CAN0(10);                          // Set CS to pin 10

void setup()
{
  Serial.begin(2000000);
  if(CAN0.begin(MCP_STDEXT, CAN_500KBPS, MCP_8MHZ) == CAN_OK) Serial.print("MCP2515 Init Okay!!\r\n");
  else Serial.print("MCP2515 Init Failed!!\r\n");
  pinMode(2, INPUT);                       // Setting pin 2 for /INT input


  // MASK 0, FILTER 0 & 1
  CAN0.init_Mask(0,0,0x07FFFFFF);                // DISABLE THESE MAILBOXES
  CAN0.init_Filt(0,0,0x0);                
  CAN0.init_Filt(1,0,0x0);                

  // MASK 1, FILTER 2 TO 5
  CAN0.init_Mask(1,0,0x07FF0000);                // FILTER BY ID 
  CAN0.init_Filt(2,0,(int32_t)CANID_PRNDL << 16);                
  CAN0.init_Filt(3,0,(int32_t)CANID_APPS << 16);                
  CAN0.init_Filt(4,0,(int32_t)CANID_VEHCHAS_F << 16);               
  CAN0.init_Filt(5,0,(int32_t)CANID_BRK << 16);       
  
  Serial.println("MCP2515 Library Mask & Filter Example...");
  CAN0.setMode(MCP_NORMAL);                // Change to normal mode to allow messages to be transmitted
}

void loop()
{
    if (!digitalRead(2))                    // If pin 2 is low, read receive buffer
    {
      CAN0.readMsgBuf(&rxId, &len, rxBuf); // Read data: len = data length, buf = data byte(s)
      if (rxId == CANID_VEHCHAS) {
         VehSpd = rxBuf[2]; 
      } else if (rxId == CANID_BRK){
         BrkAct = rxBuf[0] << 5; 
      } else if (rxId == CANID_PRNDL) {
         Gear_D = rxBuf[5] << 5; // Only D - no D3, 2 or L
      } else if (rxId == CANID_APPS) {
         AclPos = float(rxBuf[2] - 40.00) * 0.75; 
      } else if (rxId == CANID_VEHCHAS_F) {
        VehSpd2 = rxBuf[0] * 2.56 + rxBuf[1] * .01; 
      }
      //Serial.print("Brake Active: "); 
      //Serial.print(String(BrkAct));
      //Serial.print(" | Accel Pedal: "); 
      //Serial.print(String(AclPos));
      //Serial.print("% | Drive Active: "); 
      //Serial.print(String(Gear_D));
      Serial.print(" | Vehicle Spd: ");
      Serial.print(String(VehSpd2));  
      Serial.print("kph \n"); 
    }
}

/*********************************************************************************************************
END FILE
*********************************************************************************************************/
