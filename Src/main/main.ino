/* ------- General Setup ------- */
#define TBtwnPrint 2000 // Max Time between Serial Print
#define SERIAL_OUT 1
unsigned long TLastPrint = 0; 

// MCU Constants
#define CPU_VCC 3.3
#define DAC_VCC 5.0
#define VCC_5V DAC_VCC

unsigned long currMillis;
unsigned long lastMillis;
unsigned long dt_ms  = 0;

/* -------- Pedal Map Setup ------------ */
#include "MCP4725.h"
#include "Wire.h"

#define pinADC1 A0
#define pinADC2 A1

// Pedal Constant
#define VMIN1 0.7869
#define VMIN2 1.5885
#define VSLOPE1 33.9315
#define VSLOPE2 33.7067
#define K_PDLMAP 1 //shift up MODAPPS apps by this factor for driveability
#define MAXPDLTRAVEL 0.8 // max pedal travel due to pdl installation
#define APPS_OFFSET -2 // %

// Pedal Mapping Table
#define LUT_Size 9
float LUT_Brkpts[LUT_Size]  = {0, 5, 10,  20, 30,  40,  50,  60,  100};
float LUT_Val[LUT_Size]     = {0, 2, 4, 6, 8, 10, 11, 14, 100};

float Vaps1     = 0.0; // voltage at APPS1
float Vaps2     = 0.0; // Voltage at APPS2
float ActApps1  = 0.0; 
float ActApps2  = 0.0; 
float ActApps   = 0.0; 
float ModApps   = 0.0; 
float EffActApps = 0.0; // Effective Actual Pedal Position for Pdl Map purposes

MCP4725 MCP1(0x60);
MCP4725 MCP2(0x61);

/* -------- CAN Setup ------------ */
#include <mcp_can.h>
#include <SPI.h>

#define pinCANInt 2

#define CANID_VEHCHAS   1552  //0x610
#define CANID_VEHCHAS_F 176   //0xb0
#define CANID_BRK       548   //0x224
#define CANID_PRNDL     948   //0x3B4
#define CANID_APPS      947   //0x3B3  

int     VehSpd = 0;
float   VehSpd_HiRes = 0.0;
float   AclPos = 0.0;
bool    BrkAct = false;
bool    Gear_D = false;
bool    Gear_R = false;
bool    Gear_P = false;
bool    Gear_N = false;

unsigned long timestamp_lastSpdmsg = 0;
unsigned long timestamp_lastBrkmsg = 0; 
#define maxDlySpdMsg 1000 // ms 
#define maxDlyBrkMsg 1000 // ms

bool SpdMsgExpired = true;
bool BrkMsgExpired = true; 


long unsigned int rxId;
unsigned char len = 0;
unsigned char rxBuf[8];
MCP_CAN CAN0(10);                          // Set CS to pin 10

/* ------- Button and LED Setup ------- */
#define pinDnBtn 7
#define pinUpBtn 4
#define pinCCBtn 6
#define pinPMBtn 5
#define pinCCLgt 9
#define pinPMLgt 8

#define T_dbc 200
bool statUpBtn = 0; bool zstatUpBtn = 0; int durUpBtn = 0; bool alrdyDbcUpBtn = 0;
bool statDnBtn = 0; bool zstatDnBtn = 0; int durDnBtn = 0; bool alrdyDbcDnBtn = 0;
bool statCCBtn = 0; bool zstatCCBtn = 0; int durCCBtn = 0; bool alrdyDbcCCBtn = 0;
bool statPMBtn = 0; bool zstatPMBtn = 0; int durPMBtn = 0; bool alrdyDbcPMBtn = 0;

bool statCCLed = 0;
bool statPMLed = 0;

// see Cruise control section for speeds

/* ----- Cruise Control Setup ------ */
#define Kp_cc 3 // kph error -> APPS %
#define Ki_cc 1 // kph error -> APPS %
#define Kd_cc 0 // kph error -> APPS %
#define minCCSpd -1 // min Cruise control activation speed

int PID_Setpoint = 0;
double PID_Error   = 0.0;
double PID_I = 0.0;
double PID_P = 0.0;
double PID_Output = 0.0;

void setup() {
  Serial.begin(2000000);

  /* ------- Setup Pedal DAC ------- */
  MCP1.begin();
  MCP2.begin();
  Serial.println(String(MCP1.isConnected()));
  Serial.println(String(MCP2.isConnected()));

  /* ------- Setup CAN ------- */
  if (CAN0.begin(MCP_STDEXT, CAN_500KBPS, MCP_8MHZ) == CAN_OK) Serial.print("MCP2515 Init Okay!!\r\n");
  else Serial.print("MCP2515 Init Failed!!\r\n");
  pinMode(2, INPUT);                       // Setting pin 2 for /INT input


  // MASK 0, FILTER 0 & 1
  CAN0.init_Mask(0, 0, 0x07FFFFFF);              // DISABLE THESE MAILBOXES
  CAN0.init_Filt(0, 0, 0x0);
  CAN0.init_Filt(1, 0, 0x0);

  // MASK 1, FILTER 2 TO 5
  CAN0.init_Mask(1, 0, 0x07FF0000);              // FILTER BY ID
  CAN0.init_Filt(2, 0, (int32_t)CANID_PRNDL << 16);
  CAN0.init_Filt(3, 0, (int32_t)CANID_APPS << 16);
  CAN0.init_Filt(4, 0, (int32_t)CANID_VEHCHAS_F << 16);
  CAN0.init_Filt(5, 0, (int32_t)CANID_BRK << 16);

  Serial.println("MCP2515 Library Mask & Filter Example...");
  CAN0.setMode(MCP_NORMAL);                // Change to normal mode to allow messages to be transmitted

  /* ------- Setup LED and Btns ------- */
  pinMode(pinCCLgt, OUTPUT);
  pinMode(pinPMLgt, OUTPUT);
  pinMode(pinUpBtn, INPUT_PULLUP);
  pinMode(pinDnBtn, INPUT_PULLUP);
  pinMode(pinCCBtn, INPUT_PULLUP);
  pinMode(pinPMBtn, INPUT_PULLUP);

  lastMillis = millis();
  TLastPrint = lastMillis; 
}

void loop() {
  currMillis = millis();
  dt_ms = currMillis - lastMillis;
  lastMillis = currMillis;

  /* ----- CAN Section ----- */
  if (!digitalRead(pinCANInt))                    // If pin 2 is low, read receive buffer
  {
    //Serial.print("CAN Msg Rx"); 
    CAN0.readMsgBuf(&rxId, &len, rxBuf); // Read data: len = data length, buf = data byte(s)
    if (rxId == CANID_VEHCHAS) {
      VehSpd = rxBuf[2]; // Additional speed message
    }
    if (rxId == CANID_BRK) {
      BrkAct = rxBuf[0] << 5;
      timestamp_lastBrkmsg = currMillis;
    }
    if (rxId == CANID_PRNDL) {
      Gear_D = rxBuf[5] << 5; // Only D - no D3, 2 or L
    }
    if (rxId == CANID_APPS) {
      AclPos = float(rxBuf[2] - 40.00) * 0.75;
    }
    if (rxId == CANID_VEHCHAS_F) {
      VehSpd_HiRes = rxBuf[0] * 2.56 + rxBuf[1] * .01;
      timestamp_lastSpdmsg = currMillis; 
    }
    //Serial.print("Vehicle Spd: " + String(VehSpd_HiRes) + "kph \n");
  }
  
  /* ----- Btn and LED Section ----- */
  zstatUpBtn = statUpBtn; statUpBtn = !digitalRead(pinUpBtn);
  zstatDnBtn = statDnBtn; statDnBtn = !digitalRead(pinDnBtn);
  zstatCCBtn = statCCBtn; statCCBtn = !digitalRead(pinCCBtn);
  zstatPMBtn = statPMBtn; statPMBtn = !digitalRead(pinPMBtn);

  /* ----- Cruise Control Activation ----- */
  // Check Message Validity
  SpdMsgExpired = (currMillis - timestamp_lastSpdmsg) > maxDlySpdMsg; 
  BrkMsgExpired = (currMillis - timestamp_lastBrkmsg) > maxDlyBrkMsg; 
  
  // NO ACCEL OR DECEL FUNC RIGHT NOW 
  if (BrkAct | !Gear_D | SpdMsgExpired | BrkMsgExpired) {
    statCCLed = 0;
    digitalWrite(pinCCLgt, statCCLed);

  } else if (statCCBtn & zstatCCBtn & !alrdyDbcCCBtn) { // logic HI and not an edge
    durCCBtn = durCCBtn + dt_ms;
    if (durCCBtn > T_dbc & VehSpd_HiRes > minCCSpd & Gear_D & !BrkAct) { // activation criteria for CC: Speed, Gear and button
      PID_Setpoint = int(VehSpd_HiRes); // set CC Spd

      // reset PID 
      PID_I = ModApps; // initial guess of the steady state position is the current position
      PID_P = 0.0; 
      PID_Output = 0.0;
      
      statCCLed ? statCCLed = 0 : statCCLed = 1 ; // flip it
      digitalWrite(pinCCLgt, statCCLed);
      alrdyDbcCCBtn = true;
    }

  } else if (!statCCBtn & zstatCCBtn) { //reset flags and vars if falling edge
    alrdyDbcCCBtn = false;
    durCCBtn = 0;
  }

  /* ----- Pedal Mapping Activation ----- */
  if (statPMBtn & zstatPMBtn & !alrdyDbcPMBtn) { // logic HI and not an edge
    durPMBtn = durPMBtn + dt_ms;
    if (durPMBtn > T_dbc) {
      statPMLed ? statPMLed = 0 : statPMLed = 1 ; // flip it
      digitalWrite(pinPMLgt, statPMLed);
      alrdyDbcPMBtn = true;
    }
  } else if (!statPMBtn & zstatPMBtn) { //reset flags and vars if falling edge
    alrdyDbcPMBtn = false;
    durPMBtn = 0;
  }

  /* ----- Pedal Input Section ----- */
  // Obtain ADC Voltages
  Vaps1 = analogRead(pinADC1) / 1023.0 * VCC_5V * 1.0065; 
  Vaps2 = analogRead(pinADC2) / 1023.0 * VCC_5V * 1.006; 

  // Calculate Pedal Position
  ActApps1 = (Vaps1 - VMIN1) * VSLOPE1;  // %
  ActApps2 = (Vaps2 - VMIN2) * VSLOPE2;  // %
  ActApps = ((ActApps1 + ActApps2) / 2 + APPS_OFFSET)/MAXPDLTRAVEL;      // Take average, offset, scale by maxPdlTravel
  ModApps = 0;                              // %

  ActApps = min(max(ActApps, 0), 100); 


  /* ----- Pedal Output Section ----- */
  // Cruise Control Mode
  if (statCCLed) {
    PID_Error = PID_Setpoint - VehSpd_HiRes;
    PID_P = PID_Error * Kp_cc;
    PID_I = min(max(PID_I + (PID_Error * Ki_cc * dt_ms / 1000), 0), 40); // [0, 40]
    PID_Output = min(max(PID_P + PID_I, 0), 60); // [0, 60]
    ModApps = PID_Output;
  }
 
  // Pedal Map Mode
  else if (statPMLed) {
    EffActApps = ActApps * K_PDLMAP; 
    EffActApps = min(max(EffActApps, 0), 100); 
    if (EffActApps >= LUT_Brkpts[LUT_Size - 1]) {    // Clip at maximum
      ModApps = LUT_Val[LUT_Size - 1];
    }
    else if (EffActApps <= LUT_Brkpts[0]) {          // Clip at minimum
      ModApps = LUT_Val[0];
    }
    else {                                        // Interpolate
      for (int x = 0; x < LUT_Size; x = x + 1) {
        if (EffActApps >= LUT_Brkpts[x] && EffActApps <= LUT_Brkpts[x + 1]) {
          ModApps = LUT_Val[x] + (LUT_Val[x + 1] - LUT_Val[x]) * (EffActApps - LUT_Brkpts[x]) / (LUT_Brkpts[x + 1] - LUT_Brkpts[x]);
        }
      }
    }

  // Pass-thorugh Mode
  } else {
    ModApps = ActApps; 
  }

  ModApps = min(max(ModApps, 0), 100); 

  // Convert Output Apps to Voltage
  float o_Vaps1 = VMIN1 + ModApps / VSLOPE1;
  float o_Vaps2 = VMIN2 + ModApps / VSLOPE2;
  
  MCP1.setValue(o_Vaps1 / DAC_VCC * 4095);
  MCP2.setValue(o_Vaps2 / DAC_VCC * 4095);

  /* ----- Serial Output Section ----- */ 
  if (SERIAL_OUT & (currMillis - TLastPrint > TBtwnPrint)) {
    // APPS_In,CC, PM, ModApps, VehSpd
    Serial.println("Act APPS: " + String(ActApps) + "% | Eff Apps: " + String(EffActApps) + "% | CC_En: " + String(statCCLed) + " | PM_En: " + String(statPMLed) + " | Mod APPS: " + String(ModApps) + "% | VehSpd: " + String(VehSpd_HiRes) + "kph | CC_Spd: " + String(PID_Setpoint) + "kph | DT: " + String(dt_ms) + "ms"); 
    Serial.println("SpdMsgExpired: " + String(SpdMsgExpired) + " | BrkMsgExpired: " + String(BrkMsgExpired) + "| BrkAct: " +  String(BrkAct) + "TspdMsg: " + String(timestamp_lastSpdmsg) + "TbrkMsg: " + String(timestamp_lastBrkmsg)); 
    Serial.println("statCCBtn: " + String(statCCBtn)); 
    TLastPrint = currMillis;  
  }

}
