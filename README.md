# Throttle-by-Wire Project

Designing a middlebox between a vehicle APPS and ECM for torque response modification, cruise control. hill-hold and other functionality.

**Vehicle** 

- [ ] 2009 Toyota Yaris

**Functionality**

- [ ] Re-map throttle pedal for linear torque response

- [ ] Hill-hold

- [ ] Cruise-control

**Goals** 

- [ ] Embedded Programming on C/C++

- [ ] Controls Engineering

- [ ] Custom PCB Designing

- [ ] 3D Printed Enclosure
